CREATE TABLE seed_table(
    id IDENTITY PRIMARY KEY,
    seed_value varchar(10),
    is_read varchar(1));