package com.subha.kafka.route;

import org.apache.camel.CamelContext;
import org.apache.camel.PropertyInject;
import org.apache.camel.ServiceStatus;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by subha on 04/07/2018.
 */

@Component
public class GameControllerRoute extends RouteBuilder {

    private static String SEED_VALUE = "SEED_VALUE";
    private static String ID = "ID";

    @Autowired
    DataSource dataSource;

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @PropertyInject(value = "game.season.number", defaultValue = "1")
    private String numberOfSeason;

    @PropertyInject(value = "game.round.number", defaultValue = "1")
    private String numberOfRound;

    @Override
    public void configure() throws Exception {
        errorHandler(deadLetterChannel("log:gameController?level=ERROR&showHeaders=true&showBody=true&showCaughtException=true&showStackTrace=true")
                .useOriginalMessage());

        int totalNumberOfGames = Integer.parseInt(numberOfRound)* Integer.parseInt(numberOfSeason);

        restConfiguration().component("servlet")
                .bindingMode(RestBindingMode.json)
                .dataFormatProperty("prettyPrint", "true")
        ;

        from("kafka:{{kafka.from-topic}}?brokers={{kafka.server}}:{{kafka.port}}&groupId={{kafka.channel}}&autoOffsetReset=earliest&consumersCount=1")
                .routeId("read-seed")
                .setBody(simple("insert into seed_table (seed_value, is_read) values ('${body}', 'N')"))
                .to("jdbc:dataSource");


        from("timer:game?period={{game.interval}}&repeatCount=" + totalNumberOfGames)
                .routeId("game-timer")
                .autoStartup(false)
                .setBody(constant("select id, seed_value from seed_table where is_read = 'N' order by id limit 1"))
                .to("jdbc:dataSource?useHeadersAsParameters=true")
                .choice()
                .when()
                .simple("${header.CamelJdbcRowCount} > 0")
                .setHeader("GAMEID",simple("GAME${bean:execCounter.incrementAndGet}"))
                .process(exchange -> {
                    List<Map<String,String>> dataList = exchange.getIn().getBody(List.class);
                    String seed = dataList.get(0).get(SEED_VALUE);
                    exchange.getIn().setBody(seed + "-" + exchange.getIn().getHeader("GAMEID"));
                    exchange.getIn().setHeader("ID", dataList.get(0).get(ID));
                })
                .log("Seed = ${body}")
                .to("kafka:{{kafka.to-topic}}?brokers={{kafka.server}}:{{kafka.port}}")
                .setBody(simple("update seed_table set is_read = 'Y' where id = ${header.ID}"))
                .to("jdbc:dataSource?resetAutoCommit=true")
                .end();

        rest("/game-controller").description("Game Controller Service")
                .consumes("application/json").produces("application/json")

                .get("/game/{action}").description("start or stop the game").outType(String.class)
                .to("direct:game-controller");

        from("direct:game-controller")
                .choice()
                .when(simple("${header.action} =~ 'START'"))
                    .process(exchange -> {
                        CamelContext camelContext = exchange.getContext();
                        ServiceStatus serviceStatus = camelContext.getRouteStatus("game-timer");
                        if(!serviceStatus.isStarted()) {
                            exchange.getContext().startRoute("game-timer");
                            exchange.getIn().setBody("Game Started");
                        }
                        else{
                            exchange.getIn().setBody("Game Already Started");
                        }

                    })
                .when(simple("${header.action} =~ 'STOP'"))
                .to("bean:execCounter?method=set(0)")
                .process(exchange -> {
                    CamelContext camelContext = exchange.getContext();
                    ServiceStatus serviceStatus = camelContext.getRouteStatus("game-timer");
                    if(!serviceStatus.isStopped()) {
                        exchange.getContext().stopRoute("game-timer");
                        exchange.getIn().setBody("Game Stopped");
                    }
                    else{
                        exchange.getIn().setBody("Game Already Stopped");
                    }
                })
                .otherwise()
                    .setBody(constant("Action is no defined, Usage <START | STOP>"));

    }
}
