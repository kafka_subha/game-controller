package com.subha.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by subha on 04/07/2018.
 */
@SpringBootApplication
public class GameControllerApplication {

    public static void main(String... args){
        SpringApplication.run(GameControllerApplication.class, args);

    }
}
