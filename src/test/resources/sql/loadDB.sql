

CREATE TABLE seed_table(
    id INT NOT NULL GENERATED ALWAYS AS IDENTITY
	CONSTRAINT ID_PK PRIMARY KEY,
    seed_value varchar(10),
    is_read varchar(1));
