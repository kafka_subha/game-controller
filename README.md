****Game Controller Application****

This application will receive a seed from seed generator via a topic , and it will store it in database.
Then it will send the seed at a given interval to al lthe generators.

Game controller has the configuration for number of season, number of game, and interval between game.
To start/stop game call the rest api via HTTp GET.

wget http://localhost:8080/camel/game-controller/game/start
wget http://localhost:8080/camel/game-controller/game/stop

Topic configuration are defined in application.yml file.